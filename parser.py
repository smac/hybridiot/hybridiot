#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import csv
import shutil
import os

#path_dataset = "./Data/test.csv"
path_dataset = "./Data/csv_datasetConCoordEDati_noHeader.csv"
path_dataset2 = "./Data/tempo.csv" 

def parse_data():
    with open(path_dataset, 'r', newline='') as inputFile, open(path_dataset2, 'w', newline='') as writerFile:
        read_file = csv.reader(inputFile)
        write_file = csv.writer(writerFile, delimiter=';', quotechar='"')

        for row in read_file:
            row = [value for value in row[0].split(';')]
            data = [float(value) for value in row[6:len(row)]]
            write_file.writerow(row[0:6] + [data])

    columns = ['ID_station','Longitude','Latitude','Type','X','Y','Donnees']
    df = pd.read_csv(path_dataset2, header=None, names=columns, sep=';')

    #transform the df into a pickle
    df.to_pickle("./Data/df.pkl")  


def convert_data_str_to_data_float(data_str_for_a_line):
    repls = ('[', ' '), (']', '')
    text = data_str_for_a_line
    [text := text.replace(a, b) for a, b in repls]
    l_text = text.split(',')
    data_float = [float(x) for x in l_text]
    return data_float




