import copy
import random
import numpy as np
import Util
import config

from StateWindow import StateWindow
from State import State

@DeprecationWarning
class EstimatorHeterogeneous:
    predictors_score_dict_hist = {}

    predictors_score_dict = {}
    predictors_usage_count = {}

    estimates_by_predictors = {}
    best_frequency = {}

    best_frequency_by_ticks = {}

    eps = 0.05

    # the learning rate, set between 0 and 1. Setting it to 0 means that the Q-values are never updated, hence nothing
    # is learned. Setting a high value such as 0.9 means that learning can occur quickly.
    lr = 0.01
    # lr = 0.3

    gamma = 0.95

    last_agents_estimates = {}
    last_agents_real_data = {}

    best_estimates = {}

    sensing_ag_states = {}

    @classmethod
    def get_new_state(self, sensing_id, data, contextual_information: dict, tick):
        """
        Create and return a new State object with the specified input values
        :param sensing_id: the sensing agent that observes the  states
        :param _data_value: the  data observed
        :param contextual_information: the contextual information at tick
        :param tick: the current time instant
        :return: a new State object
        """
        state = State()  # create new  state
        state.tick = tick  # assign the tick
        state.data(data)  # set the  data
        state.sensing_id(sensing_id)
        return state

    @classmethod
    def eps_greedy(self, estimate_het_dict, info_count_dict, Q):
        best_predictor = -1
        max_q_value = 0
        rand = np.random.random_integers(0, 1)
        for info in estimate_het_dict:
            if info_count_dict[info] > 0:
                if rand <= self.eps:
                    best_predictor, _ = random.choice(list(estimate_het_dict.items()))
                else:
                    q_val = -(Q[info] / info_count_dict[info])
                    if q_val > max_q_value:
                        max_q_value = q_val
                        best_predictor = info
        return best_predictor

    @classmethod
    def learn(self, Q, sensing_id, best_predictor, real_data, estimate):
        if sensing_id in self.sensing_ag_states:
            state = self.sensing_ag_states[sensing_id]
        else:
            state = best_predictor
        next_state = best_predictor

        rew = np.abs(real_data - estimate)
        # https://medium.com/swlh/introduction-to-reinforcement-learning-coding-q-learning-part-3-9778366a41c0
        # SARSA policy
        Q[state] += self.lr * (rew + self.gamma * Q[next_state] - Q[state])
        return Q

    @classmethod
    def init_data_structures(self, sensing_id, estimate_het_dict):
        if sensing_id not in self.predictors_score_dict:
            self.predictors_score_dict[sensing_id] = {}  # THIS IS MY Q TABLE
            self.predictors_usage_count[sensing_id] = {}
            self.estimates_by_predictors[sensing_id] = {}
            # -1 =>  data information
            self.predictors_score_dict[sensing_id][-1] = 0
            self.predictors_usage_count[sensing_id][-1] = 0
            self.estimates_by_predictors[sensing_id][-1] = 0
            for info in estimate_het_dict:
                self.predictors_score_dict[sensing_id][info] = 0
                self.predictors_usage_count[sensing_id][info] = 0
                self.estimates_by_predictors[sensing_id][info] = 0

    @classmethod
    def do_epsilon_greedy_estimation(self, real_data, estimate_end, estimate_het_dict, sensing_id):
        """
        :param real_data:
        :param estimate_end:
        :param estimate_het_dict:
        :param sensing_id:
        :return:
        """
        self.init_data_structures(sensing_id, estimate_het_dict)

        estimates_dict = {-1: estimate_end}
        estimates_dict.update(estimate_het_dict)
        info_count_dict = self.predictors_usage_count[sensing_id]
        Q = self.predictors_score_dict[sensing_id]  # THIS IS MY Q TABLE FOR SENSING AGENT
        best_predictor = self.eps_greedy(estimates_dict, info_count_dict, Q)
        Q = self.learn(Q, sensing_id, best_predictor, real_data, estimates_dict[best_predictor])

        self.update_score_history(Q, sensing_id)

        info_count_dict[best_predictor] += 1
        self.predictors_score_dict[sensing_id].update(Q)
        self.predictors_usage_count[sensing_id].update(info_count_dict)
        for predictor in estimates_dict:
            self.estimates_by_predictors[sensing_id][predictor] = estimates_dict[predictor]
        return estimates_dict[best_predictor]

    @classmethod
    def update_score_history(self, Q, sensing_id):
        if sensing_id not in self.predictors_score_dict_hist:
            self.predictors_score_dict_hist[sensing_id] = {}
        for k, v in Q.items():
            if k not in self.predictors_score_dict_hist[sensing_id]:
                self.predictors_score_dict_hist[sensing_id][k] = []
            self.predictors_score_dict_hist[sensing_id][k].append(v)

    @classmethod
    def calculate_estimate_state(self, real_data, states_buf, states_historic, contextual_info_dict,
                                         sensing_id, tick):
        """
        Create a  state containing real and estimate information (data+  variables)
        :param real_data
        :param states_buf: current buffer of  states; when len(states_buf)
         reaches the desired length,
        it becomes a context window
        :param states_historic:
        :param contextual_info_dict:
        :param sensing_id:
        :param tick:
        :return:
        """
        # create new  state
        state = self.get_new_state(sensing_id, State.NULL_ESTIMATE, contextual_info_dict, tick)

        # do not add the state if empty (that is, without  vars)
        # if _state.is_empty() or len(states_historic) < config.NUM_DATAWINDOWS_FOR_ESTIMATION:
        if len(states_historic) < config.NUM_DATAWINDOWS_FOR_ESTIMATION:
            return None, {}

        # estimate  data
        # * estimate -> resulting estimate
        # * most_similar_tr_wins -> set of most similar  windows (considering the current one). The
        #   windows in this set are used to calcualte the  data estimate
        estimate_end, estimate_het_dict = self.estimate_data_heterogeneous(tick,
                                                                                     sensing_id,
                                                                                     states_buf,
                                                                                     states_historic)
        if estimate_end == State.NULL_ESTIMATE:
            return state, {}

        # 1. ESTIMATE WITH RL APPROACH
        estimate = self.do_epsilon_greedy_estimation(real_data, estimate_end, estimate_het_dict,
                                                    sensing_id)

        # 3. SAVE THE ESTIMATE (the one obtained from the aggr freq/predictor which minimizes the uncertainties)
        self.last_agents_estimates[sensing_id] = estimate
        self.last_agents_real_data[sensing_id] = real_data

        # 2.  UNCERTAINTIES ANALYSIS
        # self.do_aggregation_frequency_analysis(real_data, tick)

        # set the estimate to the  state
        state.set_estimated_data(estimate)
        # return the new  state, the estimated  vars as dictionary
        # where key is the ID of the  variable, the value is its numerical value.
        return state, {}

    @classmethod
    def filter_by_max_percentage_diff(self, query_array, most_similar_windows, perc=30, info_idx=-1):
        filtered = []
        for win in most_similar_windows:
            to_add = True
            if info_idx == -1:
                win_array = Util.unroll_real_or_estimate_densities(win[0].states)
            else:
                win_array = Util.unroll_real_or_estimate_dataprovider_info(win[0].states, info_idx)
            for i in range(len(query_array)):
                val_a = query_array[i]
                val_b = win_array[i]
                th_up = val_a + val_a * perc / 100
                th_down = val_a - val_a * perc / 100
                if val_b >= th_up or val_b <= th_down:
                    to_add = False
            if to_add:
                filtered.append(win)
        return filtered

    @classmethod
    def get_most_similar_states_windows(self, query_array, historic):
        """ Given a query_array (a ndarray containing  densities),
        return the most N similar windows in lookup_list
        :param query_array: a ndarray containing  densities (numerical values)
        :param lookup_list: a list of StateWindow
        :return: a list of tuples (StateWindow, float), where the
        float value is the distance between the list and the query_array."""

        # map the  states windows to a list of tuples (StateWindow, float) where
        # float is Distance(DataWindow_i,query_array)
        most_similar_windows = list(
        map(lambda x: (x,
                        Util.HybridIoT_distance(np.array(Util.unroll_real_or_estimate_data_in_state(x.states)), query_array)
                        ), historic))

        # filtered = self.filter_by_max_percentage_diff(query_array, most_similar_windows)

        # TODO: please use a sorted data structure rather than sorting at every estimation
        # remove windows which distance from query is 0
        # sorted_similar_windows = list(filter(lambda x: x[1] != 0.0, most_similar_windows))
        sorted_similar_windows = list(filter(lambda x: x[1] != 0.0, most_similar_windows))

        # sort by distance to query_array
        sorted_similar_windows = sorted(sorted_similar_windows, key=lambda tup: tup[1])

        # return the N most similar windows
        if len(sorted_similar_windows) > config.NUM_DATAWINDOWS_FOR_ESTIMATION:
            return sorted_similar_windows[:config.NUM_DATAWINDOWS_FOR_ESTIMATION]
        return sorted_similar_windows

    @classmethod
    def calculate_estimation_weight(windows_list:list):
        """
        Calculate the weight to be used for estimating a  data value
        Ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967
        :param windows_list: a list of tuple (StateWindow, float(score of dist)).
        :return:
        """
        num = 0
        den = 0
        for entry in windows_list:
            window_data = list(map(lambda x: x.data, entry[0].states))
            # take the distance of window dw to the query_array (see estimate_data)
            # the distance is the similarity between the reference context window and a similar one
            # (reference -> the one for which  data must be estimated)
            dist = entry[1]
            num = num + ((1 - dist) * (window_data[-1] - window_data[-2]))
            den = den + (1 - dist)
        return num, den

    @classmethod
    def estimate_data(self, tick, sensing_id, query_buffer, states_historic):
        """
        Estimate the  data at [tick] starting from query buffer, which is the last observed  windows
        state. The estimation is performed by a technique similar to HybridIoT (REF PLEASE)
        :param tick:
        :param sensing_id:
        :param query_buffer:
        :param states_historic:
        :return:
        """
        if len(states_historic) < config.NUM_DATAWINDOWS_FOR_ESTIMATION:
            if config.VERBOSE_LOG:
                print(f"Antenna #{sensing_id} Error, not enough windows in historic. Cannot estimate at {tick}.")
            return None, []

        # pad to obtain a homogeneous buffer where all lists have size config.data_window_size
        Util.pad_buffer(query_buffer, states_historic, config.DATAWINDOW_SIZE)

        # query_array is  a vector of  densities
        # >> map the list of States to numpy array (containing  densities)
        query_array = np.array(Util.unroll_real_or_estimate_data_in_state(query_buffer))

        # now search for the n most similar  states windows.
        # the value of n is in config file (config.NUM_DATAWINDOWS_FOR_ESTIMATION)
        windows_endogenous = self.get_most_similar_states_windows(query_array, states_historic)

        # Calculate the weight to be used for estimating the  data
        # (ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967)
        num, den = self.calculate_estimation_weight(windows_endogenous)
        if den == 0:
            return query_array[-1], []
        return query_array[-1] + (num / den), windows_endogenous

    @classmethod
    def estimate_data_heterogeneous(self, tick, sensing_id, query_buffer,
                                               states_historic):
        """
        Estimate the  data at [tick] starting from query buffer, which is the last observed  windows
        state. The estimation is performed by a technique similar to HybridIoT (REF PLEASE)
        :param tick:
        :param sensing_id:
        :param query_buffer:
        :param states_historic:
        :return:
        """
        if len(states_historic) < config.NUM_DATAWINDOWS_FOR_ESTIMATION:
            if config.VERBOSE_LOG:
                print(f"Antenna #{sensing_id} Error, not enough windows in historic. Cannot estimate at {tick}.")
            return State.NULL_ESTIMATE, {}

        # pad to obtain a homogeneous buffer where all lists have size config.data_window_size
        Util.pad_buffer(query_buffer, states_historic, config.DATAWINDOW_SIZE)

        # query_array is  a vector of  densities
        # >> map the list of States to numpy array (containing  densities)
        query_array = np.array(Util.unroll_real_or_estimate_data_in_state(query_buffer))

        # now search for the n most similar  states windows.
        # the value of n is in config file (config.NUM_DATAWINDOWS_FOR_ESTIMATION)
        windows_endogenous = self.get_most_similar_states_windows(query_array, states_historic)

        data_providers_types = [info for info in DataProvider.DATA_PROVIDER_TYPES if
                                info not in DataProvider.DATA_PROVIDER_TYPES_IGNORE_ESTIMATION]

        # Calculate the weight to be used for estimating the  data
        # (ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967)
        num, den = self.calculate_estimation_weight(windows_endogenous)
        if den:
            _data = query_array[-1] + (num / den)
        else:
            _data = query_array[-1]

        exogenous_result = {}
        for info_id in data_providers_types:
            windows_heterogeneous = self.get_most_similar_windows_by_vars(query_buffer,
                                                                                 states_historic,
                                                                                 info_id)
            num, den = self.calculate_estimation_weight(windows_heterogeneous)
            if den:
                exogenous_result[info_id] = query_array[-1] + (num / den)
            else:
                exogenous_result[info_id] = query_array[-1]

        return _data, exogenous_result
        # Calculate the weight to be used for estimating the  data
        # (ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967)
        # num, den = self.calculate_estimation_weight(windows_endogenous)
        # if den == 0:
        #    return query_array[-1], []
        # return query_array[-1] + (num / den), windows_endogenous

    @classmethod
    def calculate_real_state(self, _data_value, contextual_information_dict, sensing_id, tick):
        """Calculate the real  data at {tick} for the specified sensing agent. The contextual
        information are used to create a new State object containing the environmental
        information that have been observed in the region of sensing_id, while calculating
         data
        :param _data_value
        :param contextual_information_dict: type: dict[int, ]
        :param sensing_id:
        :param tick:
        :return:
        """
        # calculate the  data as the number of vehicle within the region of sensing_id
        # _data_value = len(SensingAGRegister.get_with_id(sensing_id).
        #                            cars_in_region())  # calculate the real  data

        # create new  state
        state = self.get_new_state(sensing_id,
                                                  _data_value,
                                                  contextual_information_dict,
                                                  tick)

        # do not add the state if empty (that is, without contextual information)
        # if _state.is_empty():
        #    return None

        return state
