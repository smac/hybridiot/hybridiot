import numpy as np
from State import State

class StateWindow:

    def __init__(self, states):
        self.states = states # type: list[State]

    def get_most_similar_windows_id(self):
        """Return the IDs of the most similar StateWindow. These windows are those that contributed to
        the estimation of the environemental informations for the last state of this window (that is, the representative
        element of this window)
        :return: a list of integer"""
        return self.states[-1].most_similar_windows_ref

    def __repr__(self):
        return f"Window [id:{id(self)}, states: {self.states}"

    def getId(self):
        return id(self)
