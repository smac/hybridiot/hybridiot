from VectorPosition import VectorPosition
import config
from State import State
from StateWindow import StateWindow
import Estimator
from copy import deepcopy
import Util

class ACA:

    def __init__(self, sensing_id:int, station_id: str , position: VectorPosition, freq: int, type_ag: str, data: list=[]):
        self.sensing_id = -sensing_id

        self.station_id = station_id

        self.position = position

        self.type_ag = type_ag

        self.__aggregate_info_counter = 0
        self.aggregation_freq = freq  # config.sensing_ag_aggregation_frequency

        # it's a window containing states
        self.state_buffer = []  # type: list[State]

        # it's the historic containing several windows
        self.states_windows = []  # type: list[StateWindow]

        # the data providers of this sensing agent in .csv
        self.oracle_data = data

        # the data perceived or estimated by the sensing agent
        self.data = []

    def __str__(self):
        return f"Sensor_ID: {self.sensing_id}, Station_ID: {self.station_id}, position: (Longitude: {self.position.longitude}, Latitude: {self.position.latitude}, X: {self.position.X}, Y: {self.position.Y}), freq: {self.aggregation_freq}, type_ag: {self.type_ag},\ndata: {self.oracle_data}"

    def on_simstep_performed(self, time):
        """
        Operation(s) to perform at each time step
        :param time: Allows to know when the agent has perceived information
        """
        # aggregate data:
        # see the next data in csv if the freq is reached
        if self.__aggregate_info_counter < self.aggregation_freq:
            self.__aggregate_info_counter += 1
        else:
            self.__aggregate_info_counter = 1
            state = self.perceiveAndAct(time=time)
            self.save_state(state)
            
    
    def perceiveAndAct(self, time): 
        """
        Depending on the operating state of the sensor associated to the agent, 
        the agent can decide to perceive the information from the sensor 
        or to estimate the environmental data that the sensor should have sent
        :param time: Allows to know when the agent has perceived information
        :return: An object State created depending on the action did
        """
        # pour savoir s'il faut estimer, on peut faire un ping sur un capteur physique 
        # pour voir s'il est encore actif sinon il faut estimer

        # add estimate method   
        value = Estimator.estimate_data(time, self.sensing_id, self.state_buffer, self.states_windows, len(self.oracle_data))

        if value != None :
            self.data.append(value)
            state = State(value, time, is_estimate= True, sensing_id = self.sensing_id)
        else:
            # Simulation method
            if time < len(self.oracle_data):
                state = State(self.oracle_data[time], time, sensing_id = self.sensing_id)
            # take in account the intermitence
            if len(self.states_windows)*config.DATAWINDOW_SIZE > int(config.DATA_BOOTSTRAP*len(self.oracle_data)):
                self.data.append(self.oracle_data[time])

        return state


    def save_state(self, state):
        """
        Save the input state into agent's state buffer.
        If this buffer is full, then a new state window (context window)
        is assembled and added to agent knowledge base
        :param state: An object State
        """

        if len(self.state_buffer) < config.DATAWINDOW_SIZE:
                self.state_buffer.append(state)
                return

        self.states_windows.append(StateWindow(deepcopy(self.state_buffer)))
        if config.CLEAR_FULL_BUFFERS:
            self.state_buffer.clear()
            self.state_buffer.append(state)
        else:
            del self.state_buffer[0]  # remove first element
            self.state_buffer.append(state)

    def lastPerception(self):
        """
        :return: the last data perceived by the agent
        """
        return self.data[-1]
    
    def lastValueBuffeur(self):
        """
        :return: the last state stored in the agent's buffer
        """
        return self.state_buffer[-1]

    def lastWindowInHistoric(self):
        """
        :return: the last window stored in the agent's historic of windows
        """
        return self.states_windows[-1]

    def last2States(self):
        """
        :return: the last 2 states stored in the agent's buffer or in the last window
        """
        if len(self.state_buffer) == 0:
            return self.states_windows[-1].states[-1], self.states_windows[-1].states[-2]
        elif len(self.state_buffer) == 1:
            return self.state_buffer[-1], self.states_windows[-1].states[-1]
        else: 
            return self.state_buffer[-1], self.state_buffer[-2]


    def is_alive(self):
        """
        Allows to know if the associated sensor to the agent is working.
        If this is the case then the agent perceives the information received by the associated sensor 
        else the agent estimates the environmental data
        :return: a boolean if True the sensor is alive else the sensor is faulty
        """
        alive = True
        lastState1, lastState2 = self.last2States()
        if (lastState1.time - lastState2.time) > 60 * 6 : # 60s * 6 to represent 6min
            alive = False
        return alive



