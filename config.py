CLEAR_FULL_BUFFERS = True

# Size of state windows assembled by sensing agents
DATAWINDOW_SIZE = 10

# minimum number of required data windows to pursue data estimation
NUM_DATAWINDOWS_FOR_ESTIMATION = 13

USE_ALL_CW_FOR_ESTIMATION = True

VERBOSE_LOG=False

DATA_BOOTSTRAP = 40/100


