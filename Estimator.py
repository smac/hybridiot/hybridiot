import copy
import random
import numpy as np
import Util
import config

from StateWindow import StateWindow

def get_most_similar_states_windows(query_array, historic):
    """ Given a query_array (a ndarray containing data),
    return the most N similar windows in historic
    :param query_array: a ndarray containing  data (numerical values)
    :param historic: a list of StateWindow
    :return: a list of tuples (StateWindow, float), where the
    float value is the distance between StateWindow and the query_array.\n
    Please note that the distance is calculated according to data values"""

    # map the  states windows to a list of tuples (StateWindow, float) where
    # float is Distance(DataWindow_i,query_array)
    windows_distances = list(
        map(lambda x: (x,
                        Util.HybridIoT_distance(np.array(Util.unroll_real_or_estimate_data_in_state(x.states)), query_array)
                        ), historic))

    # value in windows_distances -> most similar window
    # normalize values
    max_distance = max(windows_distances, key=lambda p: p[1])[1]
    most_similar_windows = [(value[0], value[1] / max_distance) for value in windows_distances]

    if config.USE_ALL_CW_FOR_ESTIMATION:
        return most_similar_windows

    # remove windows with score 0 (itself) and 1 (useless, too different)
    sorted_similar_windows = list(filter(lambda x: x[1] != 0.0 and x[1] != 1, most_similar_windows))

    # sort by distance to query_array
    # TODO: please use a sorted data structure rather than sorting at every estimation
    sorted_similar_windows = sorted(sorted_similar_windows, key=lambda tup: tup[1])

    # return the N most similar windows
    if len(sorted_similar_windows) > config.NUM_DATAWINDOWS_FOR_ESTIMATION:
        return sorted_similar_windows[:config.NUM_DATAWINDOWS_FOR_ESTIMATION]
    # HERE I RETURN THE WINDOWS WHICH DISTANCE FROM THE REFERENCE ONE IS MINIMIZED
    return sorted_similar_windows

def calculate_estimation_weight(windows_list:list):
    """
    Calculate the weight to be used for estimating a data value
    Ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967
    :param windows_list: a list of tuple (StateWindow, float(score of dist)).
    :return: Numerator and Denominator of the weight
    """
    num = 0
    den = 0
    for entry in windows_list:
        window_data = list(map(lambda x: x.data, entry[0].states))
        # take the distance of window dw to the query_array (see estimate_data)
        # the distance is the similarity between the reference context window and a similar one
        # (reference -> the one for which  density must be estimated)
        dist = entry[1]
        num = num + ((1 - dist) * (window_data[-1] - window_data[-2]))
        den = den + (1 - dist)
        # num = num + dist * (window_data[-1] - window_data[-2])
        # den = den + dist
    return num, den


def estimate_data(time, sensing_id, query_buffer, states_historic, size_data_total=0):
    """
    Estimate the data at [time] starting from query buffer, which is the last
    observed  windows state. The estimation is performed by a technique (REF PLEASE)
    :param time: Allows to know when the agent has perceived information
    :param sensing_ag_id: The ID of the agent
    :param query_buffer: The actual buffer of the agent
    :param states_historic: The actual list of StateWindow of the agent
    :param size_data_total: The total number of the real data of the agent
    :return: The estimated data
    """
    #add intermitence
    sensor_alive = True if random.randint(0, 100) <50 else False

    if (len(states_historic) < config.NUM_DATAWINDOWS_FOR_ESTIMATION) or (len(states_historic)*config.DATAWINDOW_SIZE <
     int(config.DATA_BOOTSTRAP*size_data_total)) or sensor_alive:
        if config.VERBOSE_LOG:
            print(f"Antenna #{sensing_id} Error, not enough windows in historic. Cannot estimate at {time}.")
        return None

    # pad to obtain a homogeneous buffer where all lists have size config.data_window_size
    query_buffer_copy = copy.deepcopy(query_buffer)
    Util.pad_buffer(query_buffer_copy, states_historic, config.DATAWINDOW_SIZE)
    # query_array is  a vector of  data
    # >> map the list of States to numpy array (containing  densities)
    query_array = np.array(Util.unroll_real_or_estimate_data_in_state(query_buffer_copy))

    # windows_endogenous is a list of tuples (StateWindow, float) where float
    # is the similarity score, that is, the distance between query array and the most
    # similar context window in THIS agent's knowledge base
    windows_endogenous = get_most_similar_states_windows(query_array, states_historic)

    # Calculate the weight to be used for estimating the data
    # (ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967)
    num, den = calculate_estimation_weight(windows_endogenous)
    if (not np.isfinite(den)) or den == 0:
        return None

    # if sensing_id == 0 :
    #     print("q: ", query_array[-1])
    #     print("n: ", num)
    #     print("d: ", den)
    #     print("n/d: ", (num / den))
    str0 = "Time "+ str(time)
    str1 = "query_array "+ str(query_array)
    Util.writeRowsInCSV([[str0], [str1]])
    index = 1
    for entry in windows_endogenous:
        text = "window similar "+ str(index) + " " + str(np.array(Util.unroll_real_or_estimate_data_in_state(entry[0].states)))+" Dist: "+str(entry[1])+"\n"
        Util.writeRowInCSV([text])
        index += 1
        #list_str4.append(": "+str(np.array(Util.unroll_real_or_estimate_data_in_state(entry[0].states)))+" Dist: "+str(entry[1])+" ")
    #str4 = "similar_states_windows(Window, dist) ", list_str4
    str5 = "Weight numerateur: "+ str(num)+"\n Weight denominateur: "+str(den)+"\n Weight: "+str(num/den)
    str6 = "Final Estimation: "+ str (query_array[-1] + (num / den))
    Util.writeRowsInCSV([[str5], [str6]])
    return query_array[-1] + (num / den)