import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS

class DB:
    def __init__(self, bucket:str, org:str, token:str, url:str):
        self.bucket = bucket
        self.org = org
        self.token = token
        # Store the URL of your InfluxDB instance
        self.url=url

    def conn(self):
        client = influxdb_client.InfluxDBClient(
            url=self.url,
            token=self.token,
            org=self.org
        )
        print("ready:", client.ready())
        print("health:", client.health())
        return client
    def readDB(self, query:str, client):
        # Query script
        query_api = client.query_api()
        result = query_api.query(org=self.org, query=query)
        return result
    
    def writeDB(self, client, data2Write):
       # Write script
        write_api = client.write_api(write_options=SYNCHRONOUS)
        #p = influxdb_client.Point(measurement).tag("location", location).tag("building", building).tag("king", type).tag("room", room).field(field, data)
        write_api.write(bucket=self.bucket, org=self.org, record=data2Write)

    def printRes(self, result):
        results = []
        for table in result:
            for record in table.records:
                results.append((record.get_field(), record.get_value()))
        print(results)
    
    def getInfluxdb_client(self):
        return influxdb_client