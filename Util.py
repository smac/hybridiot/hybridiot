from os.path import exists
from typing import List

import numpy as np
import pandas as pd


from State import State
from StateWindow import StateWindow
import config

import csv
import config


def is_outlier(value_to_check, data, scaling_factor):
    return get_outlier_score(value_to_check, data, scaling_factor) >= 3


def get_outlier_score(value_to_check, data, scaling_factor):
    med = np.median(data)
    mad = get_median_absolute_deviation(data)
    return abs((value_to_check - med) / mad * scaling_factor)


def get_median_absolute_deviation(data):
    med = np.median(data)

    abs_deviation = []
    for i in range(len(data)):
        abs_deviation.append(abs(data[i] - med))
    return np.median(abs_deviation)

def get_configuration():
    """Return the parameters of the simulation as a dictionary
    :return: a dictionary of {DESCRIPTION(string) : NUMBER (int)}
    """
    return {
        "num vehicles in simulation": config.TOTAL_VEHICLE_COUNTER,
        "freq. at which vehicles send FCD": config.VEH_FREQ_FIXED_VALUE,
        "% vehicles sending info at each time instant": config.VEH_PERC_SENDING_INFO,
        "size of TrafficStateWindow": config.DATAWINDOW_SIZE,
        "minimum number of TrafficStateWindow required for estimating data ": config.NUM_DATAWINDOWS_FOR_ESTIMATION,
        "Antenna aggregation frequency": config.SENSING_AG_AGGREGATION_FREQUENCY_RANGE
    }


def pad_buffer(buffer: List[State], series: List[StateWindow], w_size: int):
    """Return a padded version of the input buffer. In the output buffer, each list of floating car
     data is filled with elements from previously observed series so to obtain lists of size w_size
    :param buffer: a list of State
    :param series: a list of StateWindow
    :param w_size: the size that each buffer (that is, each buffer for information type) must have
    :return: the padded buffer, containing lists of size w_size
    """
    last_index = len(series) - 1
    s = series[last_index]  # convert the last observed series to array
    ptr_series = len(s.states) - 1  # index of the last TrafficState of s
    while len(buffer) < w_size:  # loop until buf reaches size w_size
        buffer.insert(0, s.states[ptr_series])  # insert elements from s (in a backward manner)
        ptr_series = ptr_series - 1  # go to previous element of s
        if ptr_series < 0:  # if we added all the elements of s,
            last_index = last_index - 1  # then move to the previous series
            if last_index < 0:  # if no more series available, raise an exception
                raise Exception("No more series to lookup for padding buffer.")
            s = series[last_index]  # prepare for next iteration
            ptr_series = len(s.states) - 1  # prepare for next iteration


def unroll_real_or_estimate_data_in_state(list_states:list[State]):
    """Unroll a list of State object to a list of float/integer """
    return list(map(lambda x: x.data, list_states))


def unroll_real_traffic_densities(data_window):
    """
    Unroll a list of TrafficState object to a list of integer (the real traffic densities)
    TODO: move to TrafficStateWindow
    """
    return list(map(lambda x: x.density, data_window))


def unroll_dataprovider_info(data_window, info_idx):
    """
    TODO: move to TrafficStateWindow
    """
    return list(map(lambda x: x.traffic_variables[info_idx][-1], data_window))


def is_buffer_full(buffer: dict, key: int):
    """
    Check if the buffer related to the type of input value is full (that is, if size is config.DATAWINDOW_SIZE)
    """
    if key not in buffer:
        return False
    if len(buffer[key]) < config.DATAWINDOW_SIZE:
        return False
    return True


def HybridIoT_distance(series_1: np.array, series_2: np.array):
    """
    Calculate the distance between two numerical series
    :return: a float value indicating the distance
    """
    return np.sum(np.abs(series_1 - series_2)) / series_1.size


def get_traffic_densities(traffic_state_windows):
    """

    :param traffic_state_windows: a list of TrafficStateWindow
    :return:
    """
    # from each window, composed of several TrafficState, take the last state
    list_of_last_states = list(map(lambda w: w.states[-1], traffic_state_windows))
    # then, given the list of states, take the density and the tick at which the state has been observed
    return list(map(lambda x: x.density, list_of_last_states)), list(map(lambda x: x.tick, list_of_last_states))


def get_traffic_densities_estimates(states_window_list):
    list_of_last_windows = list(map(lambda w: w.states[-1], states_window_list))
    return list(map(lambda x: x.density_estimate, list_of_last_windows)), list(
        map(lambda x: x.tick, list_of_last_windows))


def addToAverage(totalCount, totalValue, newValue):
    """ simple sliding average calculation """
    return ((1.0 * totalCount * totalValue) + newValue) / (totalCount + 1)

def writeRowInCSV(value):
    with open('output.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow(value)
    f.close()

def writeRowsInCSV(value):
    with open('output.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerows(value)
    f.close()

def evaluationByDf(agent):
    list_oracle = agent.oracle_data[config.DATAWINDOW_SIZE*config.NUM_DATAWINDOWS_FOR_ESTIMATION+1:len(agent.oracle_data)]
    list_Abs = np.abs(np.array(agent.data) - np.array(list_oracle))
    list_pre_percentage = list(map(lambda x,y: (np.abs((y-x)/y))*100 ,agent.data,list_oracle)) #MAPE
    df = pd.DataFrame(list(zip(agent.data,list_oracle,list_Abs,list_pre_percentage)), columns = ['Estimé', 'Réelle', 'Dif Absolue', 'Dif Abs Pre-Percent Error'])
    return df

def addRowInDf(df,ls):
    """
    Given a dataframe and a list, append the list as a new row to the dataframe.

    :param df: <DataFrame> The original dataframe
    :param ls: <list> The new row to be added
    :return: <DataFrame> The dataframe with the newly appended row
    """
    
    return df
