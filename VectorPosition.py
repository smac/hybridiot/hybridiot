class VectorPosition:

    def __init__(self, longitude: float=0.0, latitude: float=0.0, X: int=0, Y: int=0):
        self.longitude = longitude
        self.latitude = latitude
        self.X = X
        self.Y = Y


    def __str__(self):
        return f"Longitude: {self.longitude}, Latitude: {self.latitude}, X: {self.X}, Y: {self.Y}"